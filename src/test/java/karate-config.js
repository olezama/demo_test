function fn (){
	var config = {
		name :"Oswaldo",
		baseUrl : "https://petstore.swagger.io/v2/pet"
	};
	
	var env = karate.env;
	karate.log ('Env is : ', env)
	
	if (env == "QA"){
		config.baseUrl = "https://petstore.swagger.io/v2/pet"
	}else if(env == "DEV"){
		config.baseUrl = "https://petstore.swagger.io/v2/pet"
	}else{
		config.baseUrl = "https://petstore.swagger.io/v2/pet"
	}
	
	
	karate.configure('connectTimeout', 5000);
	karate.configure('readTimeout', 5000);
	
	return config
}