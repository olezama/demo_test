Feature: Testing Sample App from uitestingplayground

  Background: 
    * configure driver = { type: 'chrome', showDriverLog: true }

  @uiTest 
  Scenario: Sample App
    Given driver 'http://www.uitestingplayground.com/'
    And click('{}Sample App')
    And waitForUrl('http://www.uitestingplayground.com/sampleapp')
    And delay(1000)
    And input('input[name=UserName]', 'user', 100)
    And input('input[name=Password]', 'pwd', 100)
    When mouse().move('#login').click()
    * def status = text('#loginstatus')
    Then match status == 'Welcome, user!'
    And print status
    And screenshot()
    And close()
