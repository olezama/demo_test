Feature: Testing the APIs from petstore.swagger.io Creation, Request, Update and Deletion of information related to pets

  Background: 
    * header accept = 'application/json'
    * header api_key = 'special-key'
    * header Content-Type = 'application/json'
    * def urlBase = 'https://petstore.swagger.io/v2/pet/'
    * def pets = read ("pets.json")

  @apiTest 
  Scenario: GET by Status Query Test
    Given url urlBase + 'findByStatus'
    And param status = 'sold'
    When method GET
    Then status 200
    And match response[0].id !=  null
    And print response

  @apiTest 
  Scenario: Creation and verification Test
    Given url urlBase
    And request pets[0]
    When method POST
    Then status 200
    And print response
    Given url urlBase + pets[0].id
    When method GET
    Then status 200
    And match response == pets[0]
    And print response

  @apiTest 
  Scenario: Modification Test
    Given url urlBase
    And request pets[2]
    When method PUT
    Then status 200
    And print response
    Given url urlBase + pets[2].id
    When method GET
    Then status 200
    And match response == pets[2]
    And print response

  @apiTest 
  Scenario: Delete Test
    Given url urlBase + pets[2].id
    When method DELETE
    Then status 200
    And print response
    Given url urlBase + pets[2].id
    When method GET
    Then status 404
    And match response.message == 'Pet not found'
    And print response
