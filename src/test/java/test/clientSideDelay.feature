Feature: Testing Client Side Delay from uitestingplayground

  Background: 
    * configure driver = { type: 'chrome', showDriverLog: true }

  @uiTest 
  Scenario: Client Side Delay test
    Given driver 'http://www.uitestingplayground.com/'
    And click('{}Client Side Delay')
    And waitForUrl('http://www.uitestingplayground.com/clientdelay')
    When submit().click('#ajaxButton')
    And mouse().move('#ajaxButton').click()
    * retry(10).waitFor('p.bg-success')
    Then match text('p.bg-success') == 'Data calculated on the client side.'
