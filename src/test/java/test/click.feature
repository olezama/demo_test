Feature: Click Test from uitestingplayground

  Background: 
    * configure driver = { type: 'chrome', showDriverLog: true }

  @uiTest
  Scenario: Click test
    Given driver 'http://www.uitestingplayground.com/'
    And click('{}Dynamic ID')
    And waitForUrl('http://www.uitestingplayground.com/dynamicid')
    When submit().click('{}Button with Dynamic ID')
    * mouse().move('{}Button with Dynamic ID').click()
    Then close()
