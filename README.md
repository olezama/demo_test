**Table of Contents**  
- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Run the tests locally](#run-the-tests-locally)
- [Run Test in a pipeline](#run-test-in-a-pipeline)
- [Calliope](#calliope)
- [Approach used to select the scenarios](#approach-used-to-select-the-scenarios)
- [Why are they the most important](#why-are-they-the-most-important)
- [What could be the next steps to your project](#what-could-be-the-next-steps-to-your-project)



## Introduction

In this project you will find 7 test, runing using Karate Framework https://github.com/karatelabs/karate. 

To apply this tests cases we will use two test website the firts one is https://petstore.swagger.io/v2/pet/, which provides several endpoints on which you can make GET, PUT, POST and DELETE requests, and the Second one is http://www.uitestingplayground.com/ which has several user interfaces with which you can interact as well as proposed challenges


## Prerequisites

The project is developed in Java with Maven so you will need the following software to run it correctly:

* [Oracle Java 8 SDK](https://java.oracle.com)
* [Apache Maven](https://maven.apache.org)
* Your favorite IDE, including :
  * [Eclipse IDE](http://www.eclipse.org)



## Run the tests locally

To try to run the demo in the local system, enter the root folder and execute the following commands to install the dependencies and start up the test 


```
mvn clean install -DskipTests
mvn clean test 

```

## Run Test in a pipeline

Unfortunately the UI tests are not configured correctly in the gitlab pipeline so they fail even though they pass when run locally


## Calliope

As I mentioned before, these tests are based on the karate framework, so when they are completed, an HTML and json report is created, which are not currently supported on the platform. However, the link with the report that were generated with the surefire plugin is attached.

```
https://app.calliope.pro/reports/135837

```

As a point of improvement it would be nice if karate reports were included in calliope.pro


## Approach used to select the scenarios

The approach used for the selection of the tests is based on the agile methodology, delivering a viable product even if it is not the entire system.


## Why are they the most important

In that sense, the tests are carried out in the pet api to ensure its working good enough with all the methods.

## What could be the next steps to your project

The next step of the project will be the correct configuration of the pipeline as well as the generation of reports in another format such as cucumber, followed by tests of the other components.